package uz.arion.peterpartner.data.remote.currency;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Currency {

    @SerializedName("Date")
    private Date date;

    @SerializedName("PreviousDate")
    private Date previousDate;

    @SerializedName("PreviousURL")
    private String perviousUrl;

    @SerializedName("Timestampt")
    private Date timeStampt;

    @SerializedName("Valute")
    private Valute valute;

}
