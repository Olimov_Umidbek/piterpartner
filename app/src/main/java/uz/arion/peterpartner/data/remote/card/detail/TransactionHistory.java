package uz.arion.peterpartner.data.remote.card.detail;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TransactionHistory {

    private String title;
    @SerializedName("icon_url")
    private String iconUrl;
    private String date;
    private Double amount;
    private Double parsedAmount;

}
