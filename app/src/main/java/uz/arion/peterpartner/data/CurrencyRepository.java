package uz.arion.peterpartner.data;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.arion.peterpartner.config.ApiConfiguration;
import uz.arion.peterpartner.data.remote.currency.Currency;
import uz.arion.peterpartner.data.remote.currency.CurrencyService;
import uz.arion.peterpartner.ui.main.MainViewModel;

public class CurrencyRepository {

    private CurrencyService service;
    private MainViewModel viewModel;

    public CurrencyRepository(MainViewModel viewModel) {
        this.viewModel = viewModel;
        this.service = ApiConfiguration.getRetrofitCurrencyDetails().create(CurrencyService.class);
    }

    public void getCurrencyDetails() {
        viewModel.onLoading();
        Call<Currency> call = service.getCurrencyDetails();
        call.enqueue(new Callback<Currency>() {
            @Override
            public void onResponse(Call<Currency> call, Response<Currency> response) {
                switch (response.code()) {
                    case 200: {
                        viewModel.onCurrencyDetailsResponse(response.body());
                        Log.d("TAG", "onResponse: " + response.body().toString());
                    }
                    case 401: {

                    }
                    default: {

                    }
                }
            }

            @Override
            public void onFailure(Call<Currency> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
            }
        });

    }
}
