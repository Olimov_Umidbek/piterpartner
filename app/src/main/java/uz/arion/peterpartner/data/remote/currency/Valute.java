package uz.arion.peterpartner.data.remote.currency;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Valute {

    @SerializedName(value = "USD")
    private CurrencyDetails usd;
    @SerializedName(value = "GBP")
    private CurrencyDetails gbp;

}
