package uz.arion.peterpartner.data.remote.card;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import uz.arion.peterpartner.data.remote.card.detail.TransactionHistory;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Card {

    @SerializedName("card_number")
    private String cardNumber;
    @SerializedName("cardholder_name")
    private String cardHolderName;
    @SerializedName("valid")
    private String validDate;
    @SerializedName("balance")
    private Double balance;
    private Double parsedBalance;
    @SerializedName("transaction_history")
    private List<TransactionHistory> transactionHistories;

}
