package uz.arion.peterpartner.data.local;

import android.content.Context;
import android.content.SharedPreferences;

public class Prefs {

    private static final String NAME = "PeterPartner";
    private static final String KEY_DEFAULT = "default";
    private static final String KEY_CURRENCY = "currency";
    private static final String DEFAULT = "USD";
    private static final String CURRENCY = "NULL";
    private static Context context;

    public Prefs(Context context) {
        Prefs.context = context;
    }


    public static String getDefaultCurrency() {
        return context.getSharedPreferences(NAME, Context.MODE_PRIVATE).getString(KEY_DEFAULT, DEFAULT);
    }

    public static void setDefaultCurrency(String currency) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_DEFAULT, currency);
        editor.apply();
    }

    public static String getCurrency() {
        return context.getSharedPreferences(NAME, Context.MODE_PRIVATE).getString(KEY_CURRENCY, CURRENCY);
    }

    public static void setCurrency(String currency) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_CURRENCY, currency);
        editor.apply();
    }
}
