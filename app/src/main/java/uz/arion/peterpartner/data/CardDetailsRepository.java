package uz.arion.peterpartner.data;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.arion.peterpartner.config.ApiConfiguration;
import uz.arion.peterpartner.data.remote.card.Card;
import uz.arion.peterpartner.data.remote.card.CardService;
import uz.arion.peterpartner.ui.main.MainViewModel;
import uz.arion.peterpartner.utils.CurrencyConvertor;

public class CardDetailsRepository {

    private CardService service;
    private MainViewModel viewModel;

    public CardDetailsRepository(MainViewModel viewModel) {
        this.service = ApiConfiguration.getRetrofitCardDetails().create(CardService.class);
        this.viewModel = viewModel;
    }

    public void getCardDetails() {
        Call<Card> call = service.getCardDetails();
        call.enqueue(new Callback<Card>() {
            @Override
            public void onResponse(Call<Card> call, Response<Card> response) {
                switch (response.code()) {
                    case 200: {
                        response.body().setParsedBalance(CurrencyConvertor.convert(response.body().getBalance()));
                        CurrencyConvertor.convertHistoryList(response.body().getTransactionHistories());
                        viewModel.onCardDetailResponse(response.body());
                        Log.d("TAG", "onResponse: " + response.body().toString());
                        break;
                    }
                    default: {
                        viewModel.onFailure(response.code() + "");
                    }
                }
            }

            @Override
            public void onFailure(Call<Card> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
                viewModel.onFailure(t.getMessage());
            }
        });
    }
}
