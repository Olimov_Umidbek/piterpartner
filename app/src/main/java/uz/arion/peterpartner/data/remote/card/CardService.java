package uz.arion.peterpartner.data.remote.card;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CardService {

    @GET("data.php")
    Call<Card> getCardDetails();
}
