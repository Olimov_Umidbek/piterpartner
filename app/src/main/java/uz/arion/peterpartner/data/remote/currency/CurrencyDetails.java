package uz.arion.peterpartner.data.remote.currency;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyDetails {

    @SerializedName("ID")
    private String id;

    @SerializedName("NumCode")
    private Long numCode;

    @SerializedName("CharCode")
    private String charCode;

    @SerializedName("Nominal")
    private Long nominal;

    @SerializedName("Name")
    private String name;

    @SerializedName("Value")
    private Double value;

    @SerializedName("Previous")
    private Double previous;

}
