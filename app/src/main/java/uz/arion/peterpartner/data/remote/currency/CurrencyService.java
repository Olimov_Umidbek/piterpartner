package uz.arion.peterpartner.data.remote.currency;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CurrencyService {

    @GET("daily_json.js")
    Call<Currency> getCurrencyDetails();
}
