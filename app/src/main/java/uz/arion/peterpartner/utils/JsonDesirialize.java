package uz.arion.peterpartner.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import uz.arion.peterpartner.data.remote.currency.CurrencyDetails;
import uz.arion.peterpartner.data.remote.currency.Valute;

public class JsonDesirialize implements JsonDeserializer<Valute> {
    @Override
    public Valute deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = jsonElement.getAsJsonObject();
        List<CurrencyDetails> currencyDetailsList = new ArrayList<CurrencyDetails>();
        for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
//                System.out.println(entry.getKey() + " " + entry.getValue());
            // Use default deserialisation for City objects:
            CurrencyDetails currencyDetails = context.deserialize(entry.getValue(), CurrencyDetails.class);
            currencyDetailsList.add(currencyDetails);
        }
        return new Valute();
    }
}
