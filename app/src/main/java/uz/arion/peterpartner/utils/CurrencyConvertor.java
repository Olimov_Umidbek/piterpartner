package uz.arion.peterpartner.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uz.arion.peterpartner.data.local.Prefs;
import uz.arion.peterpartner.data.remote.card.detail.TransactionHistory;
import uz.arion.peterpartner.data.remote.currency.Currency;
import uz.arion.peterpartner.data.remote.currency.CurrencyDetails;
import uz.arion.peterpartner.enums.CurrencyType;

public class CurrencyConvertor {

    private static Currency currency = new Currency();
    private static Map<String, CurrencyDetails> map = new HashMap<String, CurrencyDetails>();

    public static Currency getCurrency() {
        return currency;
    }

    public static void setCurrency(Currency currency) {
        CurrencyConvertor.currency = currency;
        map.put(CurrencyType.GBP, currency.getValute().getGbp());
        map.put(CurrencyType.USD, currency.getValute().getUsd());
    }

    public static Double convert(Double amount) {
        String from = Prefs.getDefaultCurrency();
        String to = Prefs.getCurrency();
        if (from.toUpperCase().equals("RUB")) {
            CurrencyDetails details = map.get(to);
            return amount / details.getValue() * details.getNominal();
        }
        if (to.toUpperCase().equals("RUB")) {
            CurrencyDetails details = map.get(from);
            return amount / details.getNominal() * details.getValue();
        }
        CurrencyDetails first = map.get(from);
        CurrencyDetails second = map.get(to);
        Double balance = amount * first.getValue() / first.getNominal();
        return balance / second.getValue() * second.getNominal();
    }

    public static void convertHistoryList(List<TransactionHistory> historyList) {
        for (TransactionHistory history :
                historyList) {
            history.setParsedAmount(convert(history.getAmount()));
        }
    }
}
