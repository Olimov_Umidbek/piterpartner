package uz.arion.peterpartner.anim;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.vectordrawable.graphics.drawable.ArgbEvaluator;

public class Animations {

    private Context context;

    public Animations(Context context) {
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void colorChangeTransition(CardView v, int from, int to) {
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), from, to);
        colorAnimation.setDuration(1000); // milliseconds
        colorAnimation.addUpdateListener(animator -> v.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor((int) animator.getAnimatedValue()))));
        colorAnimation.start();

//        TransitionDrawable trans = new TransitionDrawable()
    }
}
