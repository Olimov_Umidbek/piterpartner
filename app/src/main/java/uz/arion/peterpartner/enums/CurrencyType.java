package uz.arion.peterpartner.enums;

public class CurrencyType {
    public final static String USD = "USD";
    public final static String GBP = "GBP";
    public final static String RUB = "RUB";

}
