package uz.arion.peterpartner.ui.main;

import android.content.Context;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;
import uz.arion.peterpartner.R;
import uz.arion.peterpartner.data.CardDetailsRepository;
import uz.arion.peterpartner.data.CurrencyRepository;
import uz.arion.peterpartner.data.local.Prefs;
import uz.arion.peterpartner.data.remote.card.Card;
import uz.arion.peterpartner.data.remote.card.detail.TransactionHistory;
import uz.arion.peterpartner.data.remote.currency.Currency;
import uz.arion.peterpartner.utils.CurrencyConvertor;

import static uz.arion.peterpartner.enums.CurrencyType.GBP;
import static uz.arion.peterpartner.enums.CurrencyType.RUB;
import static uz.arion.peterpartner.enums.CurrencyType.USD;

public class MainViewModel {

    public ObservableField<String> cardNumber = new ObservableField<>("");
    public ObservableField<String> cardHolder = new ObservableField<>("");
    public ObservableField<String> validDate = new ObservableField<>("");
    public ObservableField<String> balance = new ObservableField<>("");
    public ObservableField<String> balanceWithSymbol = new ObservableField<>("");
    public ObservableField<String> balanceAtUSD = new ObservableField<>("");
    public ObservableList<TransactionHistory> historyList = new ObservableArrayList<>();
    public ObservableField<String> currencySymbol = new ObservableField<>("");

    private Card card = new Card();
    private MainNavigator navigator;
    private CardDetailsRepository cardDetailsRepository;
    private CurrencyRepository currencyRepository;
    private Context context;


    public MainViewModel(MainNavigator navigator, Context context) {
        this.navigator = navigator;
        this.context = context;
        onLoading();
        currencyRepository = new CurrencyRepository(this);
        currencyRepository.getCurrencyDetails();
        Prefs prefs = new Prefs(context);
        setCurrencySymbol();
    }

    public void onChangeCurrencyType() {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        balance.set(df.format(CurrencyConvertor.convert(card.getBalance())));
        setCurrencySymbol();
        balanceWithSymbol.set(currencySymbol.get() + balance.get());
    }

    private void setValues() {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        cardNumber.set(card.getCardNumber());
        cardHolder.set(card.getCardHolderName());
        validDate.set(card.getValidDate());
        balanceAtUSD.set("$" + df.format(card.getBalance()));
        balance.set(df.format(card.getParsedBalance()));
        balanceWithSymbol.set(currencySymbol.get() + df.format(card.getParsedBalance()));
    }

    public void onCardDetailResponse(Card card) {
        this.card = card;
        setValues();
        historyList.addAll(card.getTransactionHistories());
        navigator.onResponse();
    }

    public void onCurrencyDetailsResponse(Currency currency) {
        CurrencyConvertor.setCurrency(currency);
        cardDetailsRepository = new CardDetailsRepository(this);
        cardDetailsRepository.getCardDetails();
//        navigator.onResponse();
    }

    public void setCurrencySymbol() {
        int symbolId = R.string.currency_pound;
        switch (Prefs.getCurrency()) {
            case GBP: {
                symbolId = R.string.currency_pound;
//                setValues();
                break;
            }
            case USD: {
                symbolId = R.string.currency_usd;
//                setValues();
                break;
            }
            case RUB: {
                symbolId = R.string.currency_rub;
//                setValues();
                break;
            }
        }
        currencySymbol.set(context.getResources().getString(symbolId));
    }

    public void onFailure(String message) {
        navigator.onFailure(message);
    }

    public void onLoading() {
        navigator.onLoading();
    }
}
