package uz.arion.peterpartner.ui.main;

public interface MainNavigator {

    void onLoading();

    void onResponse();

    void onError(String message);

    void onFailure(String message);

}
