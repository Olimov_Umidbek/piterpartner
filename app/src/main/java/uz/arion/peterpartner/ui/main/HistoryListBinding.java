package uz.arion.peterpartner.ui.main;

import android.widget.TextView;

import java.util.List;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import uz.arion.peterpartner.data.remote.card.detail.TransactionHistory;

public class HistoryListBinding {

    @BindingAdapter("app:items")
    public static void setItems(RecyclerView r, List<TransactionHistory> list) {
        MainRecyclerViewAdapter adapter = (MainRecyclerViewAdapter) r.getAdapter();
        if (adapter != null) {
            adapter.replace(list);

        }
    }

    @BindingAdapter(("app:set_text"))
    public static void setText(TextView t, String line1, String line2) {
        t.setText(new StringBuilder().append(line1).append(line1).toString());
    }
}
