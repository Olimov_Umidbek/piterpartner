package uz.arion.peterpartner.ui.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uz.arion.peterpartner.R;
import uz.arion.peterpartner.data.local.Prefs;
import uz.arion.peterpartner.databinding.ActivityMainBinding;
import uz.arion.peterpartner.enums.CurrencyType;
import uz.arion.peterpartner.ui.internet.NoInternetActivity;
import uz.arion.peterpartner.utils.NetworkConnection;

public class MainActivity extends AppCompatActivity implements MainNavigator {

    private ActivityMainBinding binding;
    private MainViewModel viewModel;
    private MainRecyclerViewAdapter adapter;
    private Prefs prefs;

    private boolean isGBPClicked = true;
    private boolean isUSDClicked = true;
    private boolean isRUBClicked = true;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Avenir-Book.ttf")
                .setFontAttrId(R.attr.fontPath).build());

        if (!new NetworkConnection(getApplicationContext()).isOnline()) {
            onFailure("Check your internet connection");
            return;
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = new MainViewModel(this, getApplicationContext());


        adapter = new MainRecyclerViewAdapter(getApplicationContext(), new ArrayList<>());

        binding.recyclerViewHistories.setAdapter(adapter);
        binding.recyclerViewHistories.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        binding.setViewModel(viewModel);

        prefs = new Prefs(getApplicationContext());
        if (Prefs.getCurrency().equals("NULL")) {
            Prefs.setCurrency("GBP");
        }

        if (Prefs.getCurrency().equals(CurrencyType.GBP)) {
            isGBPClicked = false;
        }
        if (Prefs.getCurrency().equals(CurrencyType.USD)) {
            isUSDClicked = false;
        }
        if (Prefs.getCurrency().equals(CurrencyType.RUB)) {
            isRUBClicked = false;
        }
        currencyChangeListener();

        binding.cardViewPound.setOnClickListener(e -> {
            Prefs.setCurrency(CurrencyType.GBP);
            onCurrencyChanged();
        });
        binding.cardViewUSD.setOnClickListener(e -> {
            Prefs.setCurrency(CurrencyType.USD);
            onCurrencyChanged();
        });
        binding.cardViewRUB.setOnClickListener(e -> {
            Prefs.setCurrency(CurrencyType.RUB);
            onCurrencyChanged();
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void onCurrencyChanged() {
        currencyChangeListener();
        adapter.onCurrencyChanged();
        viewModel.onChangeCurrencyType();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void currencyChangeListener() {
        switch (Prefs.getCurrency()) {
            case CurrencyType.GBP: {
                if (!isGBPClicked) {
                    binding.cardViewPound.setBackgroundTintList(
                            ColorStateList.valueOf(getResources().getColor(R.color.colorWhite)));
                    binding.imageViewCurrencyPoundChooser.setImageTintList(
                            ColorStateList.valueOf(getResources().getColor(R.color.colorNavy))
                    );
                    binding.textViewGBP.setTextColor(getResources().getColor(R.color.colorNavy));
                    isGBPClicked = true;
                    if (isUSDClicked) {
                        binding.cardViewUSD.setBackgroundTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorResolutionBlue)));
                        binding.imageViewCurrencyUSDChooser.setImageTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorWhite))
                        );
                        binding.textViewUSD.setTextColor(getResources().getColor(R.color.colorWhite));
                        isUSDClicked = false;
                    }
                    if (isRUBClicked) {
                        binding.cardViewRUB.setBackgroundTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorResolutionBlue)));
                        binding.imageViewCurrencyRUBChooser.setImageTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorWhite))
                        );
                        binding.textViewRUB.setTextColor(getResources().getColor(R.color.colorWhite));
                        isRUBClicked = false;
                    }
                }
                break;
            }
            case CurrencyType.USD: {
                if (!isUSDClicked) {
                    binding.cardViewUSD.setBackgroundTintList(
                            ColorStateList.valueOf(getResources().getColor(R.color.colorWhite)));
                    binding.imageViewCurrencyUSDChooser.setImageTintList(
                            ColorStateList.valueOf(getResources().getColor(R.color.colorNavy))
                    );
                    binding.textViewUSD.setTextColor(getResources().getColor(R.color.colorNavy));
                    isUSDClicked = true;
                    if (isGBPClicked) {
                        binding.cardViewPound.setBackgroundTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorResolutionBlue)));
                        binding.imageViewCurrencyPoundChooser.setImageTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorWhite))
                        );
                        binding.textViewGBP.setTextColor(getResources().getColor(R.color.colorWhite));
                        isGBPClicked = false;
                    }
                    if (isRUBClicked) {
                        binding.cardViewRUB.setBackgroundTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorResolutionBlue)));
                        binding.imageViewCurrencyRUBChooser.setImageTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorWhite))
                        );
                        binding.textViewRUB.setTextColor(getResources().getColor(R.color.colorWhite));
                        isRUBClicked = false;
                    }
                }
                break;
            }
            case CurrencyType.RUB: {
                if (!isRUBClicked) {
                    binding.cardViewRUB.setBackgroundTintList(
                            ColorStateList.valueOf(getResources().getColor(R.color.colorWhite)));
                    binding.imageViewCurrencyRUBChooser.setImageTintList(
                            ColorStateList.valueOf(getResources().getColor(R.color.colorNavy))
                    );
                    binding.textViewRUB.setTextColor(getResources().getColor(R.color.colorNavy));
                    isRUBClicked = true;
                    if (isUSDClicked) {
                        binding.cardViewUSD.setBackgroundTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorResolutionBlue)));
                        binding.imageViewCurrencyUSDChooser.setImageTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorWhite))
                        );
                        binding.textViewUSD.setTextColor(getResources().getColor(R.color.colorWhite));

                        isUSDClicked = false;
                    }
                    if (isGBPClicked) {
                        binding.cardViewPound.setBackgroundTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorResolutionBlue)));
                        binding.imageViewCurrencyPoundChooser.setImageTintList(
                                ColorStateList.valueOf(getResources().getColor(R.color.colorWhite))
                        );
                        binding.textViewGBP.setTextColor(getResources().getColor(R.color.colorWhite));
                        isGBPClicked = false;
                    }
                }
                break;
            }
        }
    }

    @Override
    public void onLoading() {
        binding.shimmerHistory.setVisibility(View.VISIBLE);
        binding.shimmerHistory.startShimmerAnimation();
    }

    @Override
    public void onResponse() {
        binding.shimmerHistory.stopShimmerAnimation();
        binding.shimmerHistory.setVisibility(View.GONE);
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onFailure(String message) {
        startForResult();
    }

    public void startForResult() {
        Intent intent = new Intent(this, NoInternetActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_CANCELED) {
                recreate();
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
