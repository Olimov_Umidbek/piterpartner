package uz.arion.peterpartner.ui.internet;

import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;

/**
 * Created by arion on 7/26/18.
 **/

public class NoInternetViewModel {

    public final ObservableInt progress = new ObservableInt(0);
    public final ObservableBoolean clickable = new ObservableBoolean(true);

    private final NoInternetNavigator navigator;

    public NoInternetViewModel(NoInternetNavigator navigator) {
        this.navigator = navigator;
    }

    public void connectionAction(View view) {
        progress.set(1);
//        clickable.set(false);
        navigator.onConnection();
    }

}
