package uz.arion.peterpartner.ui.internet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.dd.processbutton.iml.ActionProcessButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uz.arion.peterpartner.R;
import uz.arion.peterpartner.databinding.ActivityNoInternetBinding;
import uz.arion.peterpartner.utils.NetworkConnection;

public class NoInternetActivity extends AppCompatActivity implements NoInternetNavigator {

    private ActivityNoInternetBinding binding;
    private ActionProcessButton button;
    private NoInternetViewModel model;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_no_internet);
        model = new NoInternetViewModel(this);
        binding.setModel(model);
        button = findViewById(R.id.no_internet_btn);
        button.setMode(ActionProcessButton.Mode.PROGRESS);
//        button.setMode(ActionProcessButton.Mode.ENDLESS);

        onConnection();
    }

    @Override
    public void onConnection() {
        binding.noInternetShimmer.stopShimmerAnimation();

        if (new NetworkConnection(getApplicationContext()).isOnline()) {
            connectionSuccess();
        } else {
            connectionFail();
        }
    }

    @Override
    public void connectionSuccess() {
        binding.noInternetBtn.setProgress(100);
        binding.noInternetBtn.setClickable(true);
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public void connectionFail() {
        binding.noInternetBtn.setProgress(-1);
        binding.noInternetBtn.setClickable(true);
        binding.noInternetShimmer.startShimmerAnimation();
    }


}


