package uz.arion.peterpartner.ui.history;

import android.content.Context;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import androidx.databinding.ObservableField;
import uz.arion.peterpartner.R;
import uz.arion.peterpartner.data.local.Prefs;
import uz.arion.peterpartner.data.remote.card.detail.TransactionHistory;
import uz.arion.peterpartner.enums.CurrencyType;

public class HistoryItemViewModel {

    public ObservableField<String> title = new ObservableField<>("");
    public ObservableField<String> date = new ObservableField<>("");
    public ObservableField<String> amount = new ObservableField<>("");
    public ObservableField<String> parsedAmount = new ObservableField<>("");
    public ObservableField<Boolean> isPositive = new ObservableField<>(false);
    public ObservableField<String> symbol = new ObservableField<>("");
    private Context context;

    public HistoryItemViewModel(TransactionHistory history, Context context) {
        this.context = context;
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        title.set(history.getTitle());
        if (history.getAmount() > 0) {
            isPositive.set(true);
            symbol.set("+ " + getType());
            amount.set("$" + df.format(history.getAmount()));
        } else {
            isPositive.set(false);
            symbol.set("- " + getType());
            history.setParsedAmount(history.getParsedAmount() * -1);
            String orginalAmount = df.format(history.getAmount());
            orginalAmount = "- $" + orginalAmount.substring(1);
            amount.set(orginalAmount);
        }
        parsedAmount.set(df.format(history.getParsedAmount()));
        date.set(history.getDate());


    }


    public String getType() {
        switch (Prefs.getCurrency()) {
            case CurrencyType.GBP: {
                return context.getResources().getString(R.string.currency_pound);
            }
            case CurrencyType.USD: {
                return context.getResources().getString(R.string.currency_usd);
            }
            case CurrencyType.RUB: {
                return context.getResources().getString(R.string.currency_rub);
            }
        }
        return "";
    }


}
