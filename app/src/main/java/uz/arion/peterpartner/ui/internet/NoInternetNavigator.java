package uz.arion.peterpartner.ui.internet;

/**
 * Created by arion on 7/26/18.
 **/

public interface NoInternetNavigator {

    void onConnection();

    void connectionSuccess();

    void connectionFail();
}
