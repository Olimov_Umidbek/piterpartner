package uz.arion.peterpartner.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import uz.arion.peterpartner.R;
import uz.arion.peterpartner.data.remote.card.detail.TransactionHistory;
import uz.arion.peterpartner.databinding.HistoryListItemBinding;
import uz.arion.peterpartner.ui.history.HistoryItemViewModel;
import uz.arion.peterpartner.utils.CurrencyConvertor;

public class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.ViewHolder> {

    private final Context context;
    private List<TransactionHistory> historyList = new ArrayList<>();
    private HistoryListItemBinding binding;

    public MainRecyclerViewAdapter(Context context, List<TransactionHistory> historyList) {
        this.historyList = historyList;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        binding = DataBindingUtil.inflate(inflater, R.layout.history_list_item, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(historyList.get(position));
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    public void replace(List<TransactionHistory> historyList) {
        this.historyList.clear();
        this.historyList.addAll(historyList);
        notifyDataSetChanged();
    }

    public void onCurrencyChanged() {
        CurrencyConvertor.convertHistoryList(historyList);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private HistoryListItemBinding binding;

        public ViewHolder(@NonNull HistoryListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(TransactionHistory history) {
            HistoryItemViewModel viewModel = new HistoryItemViewModel(history, context);
            binding.setViewModel(viewModel);
            Picasso.get()
                    .load(history.getIconUrl())
                    .fit()
                    .centerInside()
                    .error(R.drawable.ic_launcher_background)
                    .into(binding.historyItemLogo);
        }

    }
}
