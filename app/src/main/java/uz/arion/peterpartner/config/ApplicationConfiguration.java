package uz.arion.peterpartner.config;

import android.app.Application;
import android.content.Context;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uz.arion.peterpartner.R;

public class ApplicationConfiguration extends Application {

    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Avenir-Book.ttf")
                .setFontAttrId(R.attr.fontPath).build());
        context = getApplicationContext();
    }
}
